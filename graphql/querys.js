import { gql } from '@apollo/client';

export const SAVE_FORM = gql`
  mutation login($input: ContactoInput!) {
    saveContacto(input: $input)
  }
`;
