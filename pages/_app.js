import Head from 'next/head';
import { ApolloProvider } from '@apollo/client';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import * as gtag from '../lib/gtag';
import { useApollo } from '../lib/apolloClient';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = url => {
      gtag.pageview(url);
    };
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);
  const apolloClient = useApollo(pageProps);
  return (
    <ApolloProvider client={apolloClient}>
      <Head>
        <meta name="viewport" content="initial-scale=1.0,width=device-width" />
        <title>Curriculum Julián Querol Polo</title>
        <meta
          key="description"
          name="description"
          content="Curriculum Julian Querol Polo"
        />
      </Head>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp;
