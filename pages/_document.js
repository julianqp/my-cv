import Document, { Html, Head, Main, NextScript } from 'next/document';
import connfig from '../config/config';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="es">
        <Head>
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${connfig.GA_TRACKING_ID}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${connfig.GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
            }}
          />
        </Head>
        <body id="body">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
