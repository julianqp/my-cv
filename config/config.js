import React from 'react';
import { AiOutlineAntDesign } from 'react-icons/ai';
import {
  FaReact,
  FaPython,
  FaHtml5,
  FaCss3Alt,
  FaBootstrap,
} from 'react-icons/fa';
import { MdSchool, MdWork, MdContactMail } from 'react-icons/md';
import { GrGraphQl } from 'react-icons/gr';
import {
  SiNextDotJs,
  SiMongodb,
  SiMysql,
  SiMaterialUi,
  SiGatsby,
  SiJava,
  SiTailwindcss,
  SiNodeDotJs,
  SiGitlab,
  SiGithub,
  SiVisualstudiocode,
  SiGitkraken,
  SiTrello,
  SiAdobexd,
} from 'react-icons/si';
import { DiJavascript1, DiGitBranch, DiDjango } from 'react-icons/di';

const config = {
  verion: '1.0.1',
  stackActual: [
    {
      color: 'text-pink-400',
      icono: <GrGraphQl />,
      nombre: 'GraphQL',
      url: 'https://graphql.org',
    },
    {
      color: 'text-green-400',
      icono: <SiNodeDotJs />,
      nombre: 'Node',
      url: 'https://nodejs.org/es/',
    },
    {
      color: 'text-blue-400',
      icono: <FaReact />,
      nombre: 'React JS',
      url: 'https://es.reactjs.org',
    },
    {
      icono: <SiNextDotJs />,
      nombre: 'Next JS',
      url: 'https://nextjs.org',
    },
    {
      color: 'text-green-500',
      icono: <SiMongodb />,
      nombre: 'Mongo DB',
      url: 'https://www.mongodb.com/es',
    },
    {
      color: 'text-yellow-500',
      icono: <DiJavascript1 />,
      nombre: 'JS',
      url: 'https://www.javascript.com',
    },
    {
      icono: <DiGitBranch />,
      nombre: 'Git',
      url: 'https://git-scm.com',
    },
  ],
  conocimientos: [
    {
      color: 'text-blue-800',
      icono: <SiMysql />,
      nombre: 'MySQL',
      url: 'https://www.mysql.com',
    },
    {
      color: 'text-yellow-400',
      icono: <FaPython />,
      nombre: 'Python',
      url: 'https://www.python.org',
    },

    {
      color: 'text-red-500',
      icono: <FaHtml5 />,
      nombre: 'HTML',
      url: 'https://developer.mozilla.org/es/docs/Web/JavaScript',
    },
    {
      icono: <SiMaterialUi />,
      nombre: 'Material',
      url: 'https://material-ui.com',
    },
    {
      color: 'text-purple-600',
      icono: <FaBootstrap />,
      nombre: 'Bootstrap',
      url: 'https://getbootstrap.com',
    },
    {
      color: 'text-blue-700',
      icono: <AiOutlineAntDesign />,
      nombre: 'Ant design',
      url: 'https://ant.design',
    },
    {
      color: 'text-blue-800',
      icono: <FaCss3Alt />,
      nombre: 'CSS',
      url: 'https://developer.mozilla.org/es/docs/Web/CSS',
    },

    {
      color: 'text-purple-700',
      icono: <SiGatsby />,
      nombre: 'Gatsby',
      url: 'https://www.gatsbyjs.com',
    },
    {
      color: 'text-blue-300',
      icono: <SiJava />,
      nombre: 'Java',
      url: 'https://www.oracle.com/es/java/technologies/',
    },
    {
      icono: <DiDjango />,
      nombre: 'Django',
      url: 'https://www.djangoproject.com',
    },
    {
      color: 'text-indigo-400',
      icono: <SiTailwindcss />,
      nombre: 'Tailwindcss',
      url: 'https://tailwindcss.com',
    },
  ],
  entornos: [
    {
      icono: <SiGithub />,
      nombre: 'GitHub',
      url: 'https://github.com',
    },
    {
      color: 'text-red-400',
      icono: <SiGitlab />,
      nombre: 'GitLab',
      url: 'https://gitlab.com',
    },
    {
      color: 'text-blue-400',
      icono: <SiVisualstudiocode />,
      nombre: 'VS-Code',
      url: 'https://code.visualstudio.com',
    },
    {
      color: 'text-indigo-400',
      icono: <SiGitkraken />,
      nombre: 'GitKraken',
      url: 'https://www.gitkraken.com',
    },
    {
      color: 'text-blue-600',
      icono: <SiTrello />,
      nombre: 'Trello',
      url: 'https://trello.com/es',
    },
    {
      color: 'text-pink-400',
      icono: <SiAdobexd />,
      nombre: 'Adobe XD',
      url: 'https://www.adobe.com/es/products/xd.html',
    },
  ],
  colors: {
    success: 'text-green-600',
    danger: 'text-red-600',
    warning: 'text-yellow-600',
    info: 'text-blue-600',
  },
  experiencia: [
    {
      empresa: 'Zerintia',
      ubicacion: 'Madrid',
      puesto: 'Desarrollador full Stack',
      labores: ['Desarrollo front-end', 'Desarrollo back-end'],
      tegnologias: ['React JS', 'Rest', 'Node', 'Mongo DB'],
      descripcion: [
        'Realizo labores de desarrollo de software enfocados a procesos industriales, ayudando a las industrias en su digitalización',
        'En el front-end uso React js y en back-end Node js con Mongo DB como base de datos.',
      ],
      metodologia: ['Agile'],
      inicio: 'marzo 2021',
    },
    {
      empresa: 'Binpar',
      ubicacion: 'Madrid',
      puesto: 'Desarrollador full Stack',
      labores: [
        'Desarrollador',
        'Gestión de proyecto',
        'Revisor entrevistas técnicas',
      ],
      tegnologias: ['React JS', 'Next JS', 'GraphQL', 'Node', 'Mongo DB'],
      descripcion: [
        'Desarrollador de páginas web en el ámbito e-commerce, con un stack basado en JS. Desempeño labores tanto en back-end como en front-end. En la parte front-end trabajo con React JS y Next JS mientras que del lado del back-end, Node JS con graphQL sobre una MongoDB.',
        'Además compagino labores de desarrollo con gestión en uno de los proyectos.',
      ],
      metodologia: ['Agile'],
      inicio: 'octubre 2019',
      fin: 'febrero 2021',
    },
    {
      empresa: 'UPM',
      ubicacion: 'Madrid',
      puesto: 'Desarrollador full Stack',
      labores: ['Desarrollador'],
      tegnologias: ['React JS', 'PHP'],
      descripcion: [
        'Desarrollo de una plataforma interna de la UPM para medir la calidad docente.',
        'En un principio desarrollaba labores de front-end con React, y a medida que avanzaba el proyecto, además desarrollos en back-end con PHP 7 apoyado de Symfony 4.',
      ],
      metodologia: ['Agile'],
      inicio: 'febrero 2019',
      fin: 'octubre 2019',
    },
    {
      empresa: 'Accenture',
      ubicacion: 'Madrid',
      puesto: 'Becario',
      labores: ['Mantenimiento de infraestructura'],
      descripcion: [
        'El inicio de las prácticas comenzaron con un curso de AWS (Amazon Web Services), aunque posteriormente, las labores cambiarían para realizar pequeñas tareas con servidores on-premise.',
      ],
      tegnologias: ['AWS'],
      inicio: 'septiembre 2018',
      fin: 'enero 2019',
    },
  ],
  formacion: [
    {
      tipo: 'Universitario',
      entidad: 'UPM',
      nombre: 'Grado en Ingeniería Informática',
      observaciones: 'Falta asignatura de inglés para título',
      modalidad: 'Presencial',
    },
    {
      tipo: 'Curso',
      entidad: 'Platzi',
      nombre: 'Curso de React js',
      modalidad: 'On-line',
    },
    {
      tipo: 'Curso',
      entidad: 'Platzi',
      nombre: 'Curso de Scrum',
      modalidad: 'On-line',
    },
    {
      tipo: 'Curso',
      entidad: 'Udemy',
      nombre: 'React JS: La biblioteca de JS creada por Facebook',
      modalidad: 'On-line',
    },
    {
      tipo: 'Curso',
      entidad: 'Red.es',
      nombre: 'Usabilidad de software y experiencia del usuario',
      modalidad: 'On-line',
    },
  ],
  idiomas: [
    {
      idioma: 'Español',
      nivel: 'Nativo',
      icono: 'ES',
    },
    {
      idioma: 'Italiano',
      nivel: 'Alto',
      icono: 'IT',
      observaciones: 'Dos años viviendo en Roma, Italia. 2010-2012',
    },
    {
      idioma: 'Inglés',
      nivel: 'B1',
      icono: 'GB',
      observaciones: 'En proceso de mejora para obtener un B2',
    },
  ],
  competencias: [
    'Aprendizaje continuo',
    'Actitud positiva',
    'Compromiso',
    'Flexibilidad',
    'Pasión',
    'Responsabilidad',
    'Solución de problemas',
    'Trabajo en equipo',
  ],
  urlApi: 'http://localhost:4000/graphql',
  GA_TRACKING_ID: '',
};

if (process.env.NODE_ENV === 'development') {
  config.urlApi = 'https://my-cv-api-dev.herokuapp.com/api';
} else if (process.env.NODE_ENV === 'production') {
  config.urlApi = 'https://my-cv-api-prod.herokuapp.com/api';
  config.GA_TRACKING_ID = 'UA-180289928-1';
}

export const ERRORES = {
  _3: 'Contraseña incorrecta.',
};

export default config;
