import React, { useState } from "react"
import { Layout, Breadcrumb } from "antd"
import Menu from "../components/Menu"
import config from "../config/config"
import { Link } from "gatsby"
import { Helmet } from "react-helmet"

const { Header, Content, Footer, Sider } = Layout

const LayoutPage = ({ children }) => {
  const [collapsed, setCollapsed] = useState(true)
  const [tam, setTam] = useState(100)

  if (!collapsed) {
    setTimeout(() => setCollapsed(true), 5000)
  }

  const onCollapse = () => {
    setCollapsed(!collapsed)
    if (tam === 200) {
      setTam(100)
    } else {
      setTam(100)
    }
  }

  const path = process.browser ? window.location.pathname : ""
  const opcion = config.menu.find(x => x.ruta === path)
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Helmet>
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=UA-180289928-1"
        ></script>
        <script>
          {`window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)}
  gtag('js', new Date());

  gtag('config', 'UA-180289928-1');`}
        </script>

        <meta charSet="utf-8" />
        <meta name="description" content="CV Julian Querol Polo" />
        <meta name="keywords" content="Trabajo, CV, FullStack" />
        <meta name="author" content="Julián Querol Polo" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <html lang="es" />
      </Helmet>
      <Sider
        style={{
          zIndex: 999,
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
        collapsible
        collapsed={collapsed}
        onCollapse={() => onCollapse()}
      >
        <div className="my-3 text-center text-white">
          <Link to="/">CV</Link>
        </div>
        <Menu />
      </Sider>
      <Layout style={{ marginLeft: 80 }} className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }} />
        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>{opcion ? opcion.nombre : ""}</Breadcrumb.Item>
          </Breadcrumb>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            {children}
          </div>
        </Content>
        <Footer className="static bg-gray-300" style={{ textAlign: "center" }}>
          CV ©{new Date().getFullYear()} Created by Julián Querol
          <p className="absolute right-0 mr-3 font-hairline">{`v${config.verion}`}</p>
        </Footer>
      </Layout>
    </Layout>
  )
}

export default LayoutPage
