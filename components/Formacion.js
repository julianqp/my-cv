import config from '../config/config';
import Titulo from './Titulo';

const Formacion = () => {
  return (
    <section id="formacion" className="text-gray-700 body-font overflow-hidden">
      <Titulo title="Formación" />
      <div className="container px-5 py-2 mx-auto">
        <div className="flex flex-wrap ">
          {config.formacion.map(elem => {
            return (
              <div
                key={elem.nombre}
                className="p-12 lg:w-1/4 md:w-1/3 flex flex-col items-start"
              >
                <span className="inline-block py-1 px-3 rounded bg-indigo-100 text-indigo-500 text-sm font-medium tracking-widest">
                  {elem.entidad}
                </span>
                <h2 className="sm:text-3xl text-2xl title-font font-medium text-gray-900 mt-4 mb-4">
                  {elem.nombre}
                </h2>
                <p>
                  Tipo: <span className="">{elem.tipo}</span>
                </p>
                {elem.modalidad && (
                  <p>
                    Modalidad: <span className="">{elem.modalidad}</span>
                  </p>
                )}
                {elem.observaciones && (
                  <div>
                    <p>
                      Observaciones:{' '}
                      <span className="leading-relaxed mb-8">
                        {elem.observaciones}
                      </span>
                    </p>
                    <p className=""></p>
                  </div>
                )}
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Formacion;
