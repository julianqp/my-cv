import Header from './Header';
import Footer from './Footer';
import AboutMe from './AboutMe';
import Experiencia from './Experiencia';
import Contacto from './Contacto';
import Tecnologias from './Tecnologias';
import Formacion from './Formacion';
import Divade from './Divade';
import Competencias from './Competencias';

const Layout = () => {
  return (
    <div className="flex flex-col min-h-screen">
      <div className="sticky top-0 bg-blue-50 w-full">
        <Header />
      </div>
      <main id="main" className="flex-grow pb-5">
        <AboutMe />
        <Divade />
        <Experiencia />
        <Divade />
        <Formacion />
        <Divade />
        <Tecnologias />
        <Divade />
        <Competencias />
        <Divade />
        <Contacto />
      </main>
      <Footer />
    </div>
  );
};

export default Layout;
