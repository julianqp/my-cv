import config from '../config/config';
import Titulo from './Titulo';

const Experiencia = () => {
  return (
    <section
      id="experiencia"
      className="text-gray-700 body-font overflow-hidden"
    >
      <Titulo title="Experiencia" />
      <div className="container px-5 py-5 mx-auto">
        <div className="flex flex-wrap">
          {config.experiencia.map(item => {
            return (
              <div key={item.empresa} className="container px-5 py-4 mx-auto">
                <div className="flex flex-col text-center w-full mb-20">
                  <h2 className="text-xl text-indigo-500 tracking-widest font-medium title-font mb-1 uppercase">
                    {item.empresa}
                  </h2>
                  <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                    {item.puesto}
                  </h1>
                  <div className="">
                    {item.descripcion.map((elem, i) => {
                      return (
                        <p
                          key={`descr-${i}`}
                          className="lg:w-2/3 mx-auto break-words leading-relaxed text-base"
                        >
                          {elem}
                        </p>
                      );
                    })}
                  </div>
                </div>
                <div className="flex flex-wrap">
                  <div className="md:w-1/2 sm:w-full px-8 py-6 border-l-2 border-gray-200">
                    <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">
                      Periodo
                    </h2>
                    <span className="mt-1 text-gray-500 text-sm capitalize">
                      {item.inicio}
                    </span>
                    <span className="mt-1 text-gray-500 text-sm"> - </span>
                    <span className="mt-1 text-gray-500 text-sm">
                      {item.fin || 'Actualmente'}
                    </span>
                  </div>
                  <div className="md:w-1/2 sm:w-full px-8 py-6 border-l-2 border-gray-200">
                    <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">
                      Labores
                    </h2>
                    <ul className="list-inside list-disc">
                      {item.labores.map((labor, i) => (
                        <li
                          key={`labor-${i}`}
                          className="leading-relaxed text-base mb-4"
                        >
                          {labor}
                        </li>
                      ))}
                    </ul>
                  </div>
                  <div className="w-full px-8 py-6 border-l-2 border-gray-200">
                    <h2 className="text-lg sm:text-xl text-gray-900 font-medium title-font mb-2">
                      Tecnologías
                    </h2>
                    <div className="flex flex-wrap mx-auto">
                      {item.tegnologias.map((elem, i) => (
                        <div key={`tecno-${i}`} className="p-2">
                          <div className="bg-gray-200 rounded flex p-4 h-full items-center">
                            <span className="title-font font-medium">
                              {elem}
                            </span>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Experiencia;
