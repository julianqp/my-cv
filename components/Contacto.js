import React, { useState } from 'react';
import Titulo from './Titulo';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ErrorMSN from './ErrorMSN';
import { SAVE_FORM } from '../graphql/querys';
import { useMutation } from '@apollo/client';
import Swal from 'sweetalert2';
import * as gtag from '../lib/gtag';

const Contacto = () => {
  const [loading, setLoading] = useState(false);
  const [saveContacto] = useMutation(SAVE_FORM);

  const formik = useFormik({
    initialValues: {
      nombre: '',
      apellidos: '',
      email: '',
      mensaje: '',
      empresa: '',
    },
    validationSchema: Yup.object({
      nombre: Yup.string().required('El nombre es obligatorio'),
      apellidos: Yup.string().required('El apellido es obligatorio'),
      email: Yup.string()
        .email()
        .lowercase()
        .required('El email es obligatorio'),
      mensaje: Yup.string().required('El mensaje es obligatorio'),
      empresa: Yup.string().default('-'),
    }),
    onSubmit: async (valores, reset) => {
      setLoading(true);
      const { data } = await saveContacto({
        variables: {
          input: valores,
        },
      });

      if (data && data.saveContacto) {
        Swal.fire({
          title: 'Completado',
          text: 'El mensaje ha sido enviado.',
          icon: 'success',
          showCancelButton: false,
        });

        gtag.event({
          action: 'submit_form',
          category: 'Formulario Contacto',
          label: `Nuevo formulario: ${valores.email} ; ${valores.nombre} ${valores.apellidos}`,
        });

        reset.resetForm({
          nombre: '',
          apellidos: '',
          email: '',
          mensaje: '',
          empresa: '',
        });
      } else {
        Swal.fire(
          'Cancelado',
          'Ha habido un error inesperado. Inténtelo más tarde.',
          'error',
        );
      }
      setLoading(false);
    },
  });
  return (
    <section id="contacto" className="text-gray-700 body-font ">
      <Titulo title="Contacto" />
      <div className="container px-5 mx-auto">
        <div className="flex flex-col text-center w-full mb-12">
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Contacta conmigo a través del formulario y responderé a la mayor
            brevedad posible.
          </p>
        </div>
        <div className="lg:w-1/2 md:w-2/3 mx-auto">
          <form
            className="flex flex-wrap "
            onSubmit={e => {
              e.preventDefault();

              formik.handleSubmit();
            }}
          >
            <div className="p-2 w-full md:w-1/2">
              <div className="">
                <label
                  htmlFor="name"
                  className="leading-7 text-sm text-gray-600"
                >
                  Nombre
                </label>
                <input
                  value={formik.values.nombre}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  type="text"
                  id="name"
                  name="nombre"
                  className="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                />
                {formik.touched.nombre && formik.errors.nombre && (
                  <ErrorMSN texto={formik.errors.nombre} />
                )}
              </div>
            </div>
            <div className="p-2 w-full md:w-1/2">
              <div className="">
                <label
                  htmlFor="lastname"
                  className="leading-7 text-sm text-gray-600"
                >
                  Apellidos
                </label>
                <input
                  value={formik.values.apellidos}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  type="text"
                  id="lastname"
                  name="apellidos"
                  className="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                />
                {formik.touched.apellidos && formik.errors.apellidos && (
                  <ErrorMSN texto={formik.errors.apellidos} />
                )}
              </div>
            </div>
            <div className="p-2 w-full md:w-1/2">
              <div className="">
                <label
                  htmlFor="email"
                  className="leading-7 text-sm text-gray-600"
                >
                  Email
                </label>
                <input
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  type="email"
                  id="email"
                  name="email"
                  className="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                />
                {formik.touched.email && formik.errors.email && (
                  <ErrorMSN texto={formik.errors.email} />
                )}
              </div>
            </div>
            <div className="p-2 w-full md:w-1/2">
              <div className="">
                <label
                  htmlFor="name"
                  className="leading-7 text-sm text-gray-600"
                >
                  Empresa
                </label>
                <input
                  value={formik.values.empresa}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  type="text"
                  id="name"
                  name="empresa"
                  className="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                />
                {formik.touched.empresa && formik.errors.empresa && (
                  <ErrorMSN texto={formik.errors.empresa} />
                )}
              </div>
            </div>
            <div className="p-2 w-full">
              <div className="">
                <label
                  htmlFor="message"
                  className="leading-7 text-sm text-gray-600"
                >
                  Mensaje
                </label>
                <textarea
                  value={formik.values.mensaje}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  id="message"
                  name="mensaje"
                  className="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"
                ></textarea>
                {formik.touched.mensaje && formik.errors.mensaje && (
                  <ErrorMSN texto={formik.errors.mensaje} />
                )}
              </div>
            </div>
            <div className="p-2 w-full">
              <button
                disabled={loading}
                type="submit"
                className={`flex items-center mx-auto text-white ${
                  loading ? ' cursor-wait disabled:opacity-50 ' : ''
                } bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg`}
              >
                {loading ? (
                  <svg
                    className="animate-spin m-1 h-5 w-5 text-white"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                  >
                    <circle
                      className="opacity-25"
                      cx="12"
                      cy="12"
                      r="10"
                      stroke="currentColor"
                      strokeWidth="4"
                    ></circle>
                    <path
                      className="opacity-75"
                      fill="currentColor"
                      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                  </svg>
                ) : (
                  'Enviar'
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

export default Contacto;
