import React, { useState } from 'react';
import useMenu from '../hooks/useMenu';
import useResize from '../hooks/useResize';
import scrollView from '../utils/scrollView';

const classSelected = 'text-blue-500';
const MobileSelected = 'bg-blue-200';

const menuOptions = {
  'sobre-mi': 'Sobre mi',
  experiencia: 'Experiencia',
  formacion: 'Formación',
  tecnologias: 'Tecnologías',
  competencias: 'Competencias',
  contacto: 'Contacto',
};

const MenuMobile = ({ onClose, selected }) => {
  document.documentElement.style.overflow = 'hidden';
  document.body.scroll = 'no';
  const cerrar = () => {
    document.documentElement.style.overflow = 'scroll';
    document.body.scroll = 'yes';
    onClose();
  };
  return (
    <div className="absolute overflow-auto top-0 left-0 w-full h-screen flex flex-col pt-5">
      <button
        onClick={() => {
          cerrar();
        }}
        className="flex justify-center p-5"
      >
        <svg
          className="w-10 h-10 text-white p-2 bg-black rounded-full"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M6 18L18 6M6 6l12 12"
          ></path>
        </svg>
      </button>
      <button
        onClick={() => {
          scrollView('sobre-mi');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'sobre-mi' ? MobileSelected : ''
        }`}
      >
        Sobre mi
      </button>
      <button
        onClick={() => {
          scrollView('experiencia');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'experiencia' ? MobileSelected : ''
        }`}
      >
        Experiencia
      </button>
      <button
        onClick={() => {
          scrollView('formacion');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'formacion' ? MobileSelected : ''
        }`}
      >
        Formación
      </button>
      <button
        onClick={() => {
          scrollView('tecnologias');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'tecnologias' ? MobileSelected : ''
        }`}
      >
        Tecnologías
      </button>
      <button
        onClick={() => {
          scrollView('competencias');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'competencias' ? MobileSelected : ''
        }`}
      >
        Competencias
      </button>
      <button
        onClick={() => {
          scrollView('contacto');
          cerrar();
        }}
        className={`hover:text-gray-900 mx-12 my-1 p-2 rounded-md border border-blue-500 ${
          selected === 'contacto' ? MobileSelected : ''
        }`}
      >
        Contacto
      </button>
    </div>
  );
};

const Header = () => {
  const [view, setView] = useState(false);
  const selected = useMenu();
  const size = useResize();

  if (size.width <= 775) {
    return (
      <div key={`menu-${selected}`} id="menu" className="relative">
        {view && (
          <MenuMobile selected={selected} onClose={() => setView(false)} />
        )}
        <header className="text-gray-700 body-font">
          <div className="container mx-auto flex justify-between p-5 items-center">
            <p className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
              <span className="ml-3 text-xl p-2 text-white bg-black rounded-full">
                JQP
              </span>
            </p>
            <p className={`mr-5 hover:text-gray-900 ${classSelected}`}>
              {menuOptions[selected]}
            </p>
            <button onClick={() => setView(true)}>
              <svg
                className="w-6 h-6"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </button>
          </div>
        </header>
      </div>
    );
  }
  return (
    <div id="menu" className="">
      <header className="text-gray-700 body-font">
        <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
          <p className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <span className="ml-3 text-xl p-2 text-white bg-black rounded-full">
              JQP
            </span>
          </p>
          <nav className="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
            <button
              onClick={() => scrollView('sobre-mi')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'sobre-mi' ? classSelected : ''
              }`}
            >
              Sobre mi
            </button>
            <button
              onClick={() => scrollView('experiencia')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'experiencia' ? classSelected : ''
              }`}
            >
              Experiencia
            </button>
            <button
              onClick={() => scrollView('formacion')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'formacion' ? classSelected : ''
              }`}
            >
              Formación
            </button>
            <button
              onClick={() => scrollView('tecnologias')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'tecnologias' ? classSelected : ''
              }`}
            >
              Tecnologías
            </button>
            <button
              onClick={() => scrollView('competencias')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'competencias' ? classSelected : ''
              }`}
            >
              Competencias
            </button>
            <button
              onClick={() => scrollView('contacto')}
              className={`mr-5 hover:text-gray-900 ${
                selected === 'contacto' ? classSelected : ''
              }`}
            >
              Contacto
            </button>
          </nav>
        </div>
      </header>
    </div>
  );
};

export default Header;
