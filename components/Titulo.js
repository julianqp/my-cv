const Titulo = ({ title }) => {
  return (
    <div className="flex flex-col my-5">
      <h1 className="text-center text-3xl uppercase font-bold text-blue-400">
        {title}
      </h1>
    </div>
  );
};

export default Titulo;
