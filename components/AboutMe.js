import { FaFilePdf } from 'react-icons/fa';
import Testimonial from './Testimonial';
import Titulo from './Titulo';

const AboutMe = () => {
  return (
    <section id="sobre-mi" className="text-gray-700 body-font">
      <Titulo title="Sobre mi" />
      <div className="container px-5 mx-auto flex flex-col">
        <div className="lg:w-4/6 mx-auto">
          <div className="flex flex-col sm:flex-row">
            <div className="sm:w-1/3 text-center sm:pr-8 sm:py-8">
              <div className="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
                <img
                  className="object-cover object-center rounded-full inline-block border-2"
                  src="perfil.png"
                />
              </div>
              <div className="flex flex-col items-center text-center justify-center">
                <h2 className="font-medium title-font mt-4 text-gray-900 text-lg">
                  Julián Querol Polo
                </h2>
                <div className="w-12 h-1 bg-indigo-500 rounded mt-2 mb-4"></div>
                <div className="text-left">
                  <p className="text-base text-gray-600">
                    <b>Email:</b> julian.querol.p@gmail.com
                  </p>
                  <p className="text-base text-gray-600">
                    <b>Teléfono:</b> +34 634674896
                  </p>
                  <p className="text-base text-gray-600">Carné de conducir</p>
                  <p className="text-base">
                    <a
                      className="text-blue-500"
                      href="https://github.com/julianqp"
                      target="_blank"
                    >
                      Github
                    </a>
                  </p>
                  <p className="text-base">
                    <a
                      className="text-blue-500"
                      href="https://gitlab.com/julianqp"
                      target="_blank"
                    >
                      Gitlab
                    </a>
                  </p>
                  <p className="text-base">
                    <a
                      className="text-blue-500"
                      href="https://bitbucket.org/julianqp/"
                      target="_blank"
                    >
                      Bitbucket
                    </a>
                  </p>
                  <p className="text-base text-gray-600">
                    <a
                      className="text-blue-500"
                      href="https://www.linkedin.com/in/julianquerolpolo/"
                      target="_blank"
                    >
                      LinkedIn
                    </a>
                  </p>
                  <p className="flex items-center text-base text-gray-600 hover:underline hover:text-blue-500">
                    <a
                      href="documents/CV_Julian_Querol_Polo.pdf"
                      target="_blank"
                      download
                    >
                      Descargar CV
                    </a>
                    <span className="text-red-600 ml-2">
                      <FaFilePdf />
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div className="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-gray-300 sm:border-t-0 border-t mt-4 pt-4 sm:mt-0 text-center sm:text-left">
              <h1 className="mb-10 text-3xl">Quién soy</h1>
              <p className="leading-relaxed text-lg mb-4">
                Soy un chico simpático, tranquilo pero divertido, apasinado de
                la tecnología, razón por la cual decidí estudiar Ingeniería
                Informática en la UPM. Siempre que puedo intento aprender nuevas
                tercnologías o metodologías que me ayuden a mejorar y ser más
                productivo.
              </p>
              <p className="leading-relaxed text-lg mb-4">
                Actualmente estoy trabajando en{' '}
                <a href="https://zerintia.com" target="_blank">
                  Zerintia
                </a>
                , empresa enfocada en el desarrollo de software para la
                industria.
              </p>
              <p className="leading-relaxed text-lg mb-4">
                En mi tiempo libre me gusta ir al cine, viajar, hacer excusiones
                o practicar deportes como el fútbol y el pádel (puedes
                encontrarme a través de{' '}
                <a
                  className="text-blue-500"
                  href="https://playtomic.io"
                  target="_blank"
                >
                  playtomic
                </a>
                ).
              </p>
              <p className="leading-relaxed text-lg mb-4">
                Si en mi vida tuviera que elegir la experiencia que más me ha
                cambiado, sin duda elegiría el momento en el que me fuí a vivir
                a Roma, Italia, con 17 años, sin apenas hablar el idioma. La
                experiencia duró dos años.
              </p>
            </div>
          </div>
          <Testimonial />
        </div>
      </div>
    </section>
  );
};

export default AboutMe;
