import config from '../config/config';
import Titulo from './Titulo';
const Tecnologias = () => {
  return (
    <section id="tecnologias" className="text-gray-700 body-font">
      <Titulo title="Tecnologías" />
      <div className="container px-5 py-8 mx-auto">
        <div className="flex flex-col text-center w-full mb-20">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
            Stack Laboral Actual
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Herramientas con las que actualmente despeño mi trabajo
          </p>
        </div>
        <div className="flex flex-wrap ">
          {config.stackActual.map(item => {
            return (
              <div
                key={item.nombre}
                className="p-2 xl:w-1/6 lg:w-1/4 md:w-1/4 sm:w-1/2 w-full "
              >
                <a href={item.url} target="_blank">
                  <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                    <div
                      className={`text-3xl ${
                        item.color ? item.color : ''
                      } pr-4`}
                    >
                      {item.icono}
                    </div>
                    <div className="flex-grow">
                      <h2 className="text-gray-900 title-font font-medium">
                        {item.nombre}
                      </h2>
                    </div>
                  </div>
                </a>
              </div>
            );
          })}
        </div>
      </div>
      <div className="container px-5 py-8 mx-auto">
        <div className="flex flex-col text-center w-full mb-20">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
            Conocimientos
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Herramientas usadas en el ámbito personal o académico
          </p>
        </div>
        <div className="flex flex-wrap ">
          {config.conocimientos.map(item => {
            return (
              <div
                key={item.nombre}
                className="p-2 xl:w-1/6 lg:w-1/4 md:w-1/4 sm:w-1/2 w-full "
              >
                <a href={item.url} target="_blank">
                  <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                    <div
                      className={`text-3xl ${
                        item.color ? item.color : ''
                      } pr-4`}
                    >
                      {item.icono}
                    </div>
                    <div className="flex-grow">
                      <h2 className="text-gray-900 title-font font-medium">
                        {item.nombre}
                      </h2>
                    </div>
                  </div>
                </a>
              </div>
            );
          })}
        </div>
      </div>
      <div className="container px-5 py-8 mx-auto">
        <div className="flex flex-col text-center w-full mb-20">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
            Entornos
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Entornos que utilizo en mi trabajo o mi tiempo libre
          </p>
        </div>
        <div className="flex flex-wrap ">
          {config.entornos.map(item => {
            return (
              <div
                key={item.nombre}
                className="p-2 xl:w-1/6 lg:w-1/4 md:w-1/4 sm:w-1/2 w-full "
              >
                <a href={item.url} target="_blank">
                  <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                    <div
                      className={`text-3xl ${
                        item.color ? item.color : ''
                      } pr-4`}
                    >
                      {item.icono}
                    </div>
                    <div className="flex-grow">
                      <h2 className="text-gray-900 title-font font-medium">
                        {item.nombre}
                      </h2>
                    </div>
                  </div>
                </a>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Tecnologias;
