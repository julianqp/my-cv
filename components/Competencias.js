import Titulo from './Titulo';
import config from '../config/config';
import Flag from 'react-world-flags';

const Competencias = () => {
  return (
    <section id="competencias" className="text-gray-600 body-font">
      <Titulo title="Competencias" />
      <div className="container flex flex-wrap lg:w-4/5 py-4 sm:mx-auto sm:mb-2">
        {config.competencias.map(item => {
          return (
            <div key={item} className="p-2 sm:w-1/2 md:w-1/3 lg:w-1/4 w-full">
              <div className="bg-gray-100 rounded flex p-4 h-full items-center">
                <svg
                  fill="none"
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="3"
                  className="text-indigo-500 w-6 h-6 flex-shrink-0 mr-4"
                  viewBox="0 0 24 24"
                >
                  <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
                  <path d="M22 4L12 14.01l-3-3"></path>
                </svg>
                <span className="title-font font-medium">{item}</span>
              </div>
            </div>
          );
        })}
      </div>
      <div className="container px-5 mx-auto">
        <div className="flex flex-col text-center w-full">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
            Idiomas
          </h1>
        </div>
        <div className="flex flex-wrap">
          {config.idiomas.map(item => {
            return (
              <div key={item.idioma} className="p-2 lg:w-1/3 md:w-1/2 w-full">
                <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                  <Flag
                    className="lg:w-16 lg:h-16 md:w-12 md:h-12 w-8 h-8 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
                    code={item.icono}
                    alt={item.idioma}
                  />
                  <div className="flex-grow">
                    <h2 className="text-gray-900 title-font font-medium">
                      {item.idioma}
                    </h2>
                    <p className="text-gray-500">{item.nivel}</p>
                    <p className="text-gray-500">{item.observaciones}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Competencias;
