const ErrorMSN = ({ texto }) => {
  return <p className="pt-2 text-xs text-red-700">{texto}</p>;
};

export default ErrorMSN;
