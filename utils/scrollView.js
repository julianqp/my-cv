const scrollIntoView = id => {
  const yOffset = document.getElementById('menu').offsetHeight;
  const element = document.getElementById(id);
  const y = element.getBoundingClientRect().top + window.pageYOffset - yOffset;

  window.scrollTo({ top: y, behavior: 'smooth' });
  /*
  const element = document.getElementById(id);
  element.scrollIntoView({ behavior: 'smooth', alignToTop: false });
*/
};

export default scrollIntoView;
