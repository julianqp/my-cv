import { useEffect, useState } from 'react';

const defaultMenu = 'sobre-mi';

const useMenu = () => {
  const [select, setSelect] = useState(defaultMenu);
  const useScroll = () => {
    let newSelect = '';
    // Obtengo los recursos necesarios
    const sobreMi = document.getElementById('sobre-mi');
    const experiencia = document.getElementById('experiencia');
    const tecnologias = document.getElementById('tecnologias');
    const formacion = document.getElementById('formacion');
    const competencias = document.getElementById('competencias');

    const body = document.getElementById('body');

    // Calculo la distancia final de cada componente, excepto el último
    const calSobreMi = sobreMi.offsetHeight + sobreMi.offsetTop;
    const calexperiencia = experiencia.offsetHeight + experiencia.offsetTop;
    const calcompetencias = competencias.offsetHeight + competencias.offsetTop;
    const calconocimientos = tecnologias.offsetHeight + tecnologias.offsetTop;
    const calcdormacion = formacion.offsetHeight + formacion.offsetTop;
    // Calculo el scroll maximo
    const scrollMax = body.offsetHeight - window.innerHeight;
    // Calculo el height máximo
    const view = body.offsetHeight;
    // Calculo la relación entre el scroll y el tamaño de la vista
    const calScroll = view / scrollMax;
    // Calcula la altura exacta del scroll con referencia a la pantalla
    const relScroll = Math.round(window.scrollY * calScroll);

    if (calSobreMi > relScroll) {
      newSelect = 'sobre-mi';
    } else if (calexperiencia > relScroll) {
      newSelect = 'experiencia';
    } else if (calcdormacion > relScroll) {
      newSelect = 'formacion';
    } else if (calconocimientos > relScroll) {
      newSelect = 'tecnologias';
    } else if (calcompetencias > relScroll) {
      newSelect = 'competencias';
    } else {
      newSelect = 'contacto';
    }
    // Actualizamos en caso de que seán distintos
    setSelect(newSelect);
  };
  useEffect(() => {
    useScroll();
    document.addEventListener('scroll', useScroll);
    return () => {
      document.removeEventListener('scroll', useScroll);
    };
  }, []);
  return select;
};

export default useMenu;
