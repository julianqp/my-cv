import { useEffect, useState } from 'react';

const defaultSize = {
  width: 0,
  height: 0,
};

const useResize = () => {
  const [size, setSize] = useState(defaultSize);

  const resize = () => {
    setSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  useEffect(() => {
    resize();
    window.addEventListener('resize', resize);
    return () => {
      window.addEventListener('resize', resize);
    };
  }, []);
  return size;
};

export default useResize;
